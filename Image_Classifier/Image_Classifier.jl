### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 3fdcb9d0-d20d-11eb-2ae0-2fd94ecfc689
begin
	
	using Flux.Data: DataLoader
	using Flux.Optimise: Optimiser, WeightDecay
	using Flux: onehotbatch, onecold
	using Flux.Losses: logitcrossentropy
	using Statistics, Random
	using Logging: with_logger
	using TensorBoardLogger: TBLogger, tb_overwrite, set_step!, set_step_increment!
	using ProgressMeter: @showprogress
	using MLDatasets 
	import BSON
	using CUDA
	using PlutoUI
	
end

# ╔═╡ deedc6a0-61b2-45ec-ba36-2be46629e3d0
function CNN_Type_AlexNet(; imgsize=(28,28,1), nclasses=10) 
    out_conv_size = (imgsize[1]÷4 - 3, imgsize[2]÷4 - 3, 16)
    
    return Chain(
            Conv((5, 5), imgsize[end]=>6, relu),
            MaxPool((2, 2)),
            Conv((5, 5), 6=>16, relu),
            MaxPool((2, 2)),
            flatten,
            Dense(prod(out_conv_size), 120, relu), 
            Dense(120, 84, relu), 
            Dense(84, nclasses)
          )
end 

# ╔═╡ 220be46e-2607-4d86-8e57-28be415842a1
begin
	loss(ŷ, y) = logitcrossentropy(ŷ, y)
	num_params(model) = sum(length, Flux.params(model)) 
	round4(x) = round(x, digits=4)
	
	
	Base.@kwdef mutable struct Args
    η = 3e-4             # learning rate
    λ = 0                # L2 regularizer param, implemented as weight decay
    batchsize = 128      # batch size
    epochs = 10          # number of epochs
    seed = 0             # set seed > 0 for reproducibility
    use_cuda = true      # if true use cuda (if available)
    infotime = 1 	     # report every `infotime` epochs
    checktime = 5        # Save the model every `checktime` epochs. Set to 0 for no checkpoints.
    tblogger = true      # log training with tensorboard
    savepath = "training_Runs/"    # results path
end
end

# ╔═╡ a604047e-7c0c-48f2-b4e6-4a99b6005b05
function eval_loss_accuracy(loader, model, device)
    l = 0f0
    acc = 0
    ntot = 0
    for (x, y) in loader
        x, y = x |> device, y |> device
        ŷ = model(x)
        l += loss(ŷ, y) * size(x)[end]        
        acc += sum(onecold(ŷ |> cpu) .== onecold(y |> cpu))
        ntot += size(x)[end]
    end
    return (loss = l/ntot |> round4, acc = acc/ntot*100 |> round4)
end

# ╔═╡ 6c480c9e-9967-414b-b79b-05ae2a6d36db
function Rdata(args)     
	
	xtrain, ytrain = MNIST.traindata(Float32, dir="./Main_DataSet")
	xtest, ytest = MNIST.testdata(Float32, dir="./Main_DataSet")

    xtrain = reshape(xtrain, 28, 28, 1, :)
    xtest = reshape(xtest, 28, 28, 1, :)
	
    ytrain, ytest = onehotbatch(ytrain, ["normal","pneumonia"]), onehotbatch(ytest,["normal","pneumonia"])

    train_loader = DataLoader((xtrain, ytrain), batchsize=args.batchsize, shuffle=true)
    test_loader = DataLoader((xtest, ytest),  batchsize=args.batchsize)
    
    return train_loader, test_loader
end

# ╔═╡ b1929e61-9e28-40fd-bece-b081ab38e91f
function start(; kws...)
    args = Args(; kws...)
    args.seed > 0 && Random.seed!(args.seed)
    use_cuda = args.use_cuda && CUDA.functional()
    
    if use_cuda
        device = gpu
    else
        device = cpu
    end

    ## DATA
    train_loader, test_loader = Rdata(args)

    ## MODEL AND OPTIMIZER
    model = LeNet5() |> device
    
    ps = Flux.params(model)  

    opt = ADAM(args.η) 
    if args.λ > 0 # add weight decay, equivalent to L2 regularization
        opt = Optimiser(WeightDecay(args.λ), opt)
    end
    
    ## LOGGING UTILITIES
    if args.tblogger 
        tblogger = TBLogger(args.savepath, tb_overwrite)
        set_step_increment!(tblogger, 0) # 0 auto increment since we manually set_step!
        @info "logs \"$(args.savepath)\""
    end
    
    function report(epoch)
        train = eval_loss_accuracy(train_loader, model, device)
        test = eval_loss_accuracy(test_loader, model, device)        
        println("Epoch: $epochTest: $(test)")
        if args.tblogger
            set_step!(tblogger, epoch)
            with_logger(tblogger) do
                @info "train" loss=train.loss  acc=train.acc
                @info "test"  loss=test.loss   acc=test.acc
            end
        end
    end
    
    ## TRAINING
    @info "Start Training"
    report(0)
    for epoch in 1:args.epochs
        @showprogress for (x, y) in train_loader
            x, y = x |> device, y |> device
            gs = Flux.gradient(ps) do
                    ŷ = model(x)
                    loss(ŷ, y)
                end

            Flux.Optimise.update!(opt, ps, gs)
        end
        
        ## Printing and logging
        epoch % args.infotime == 0 && report(epoch)
        if args.checktime > 0 && epoch % args.checktime == 0
            !ispath(args.savepath) && mkpath(args.savepath)
            modelpath = joinpath(args.savepath, "model.bson") 
            let model = cpu(model) #return model to cpu before serialization
                BSON.@save modelpath model epoch
            end
            @info "Model saved in \"$(modelpath)\""
        end
    end
end

# ╔═╡ 982d8080-bf99-48a1-b649-ffb4a32b9783
start()

# ╔═╡ Cell order:
# ╠═3fdcb9d0-d20d-11eb-2ae0-2fd94ecfc689
# ╠═deedc6a0-61b2-45ec-ba36-2be46629e3d0
# ╠═220be46e-2607-4d86-8e57-28be415842a1
# ╠═a604047e-7c0c-48f2-b4e6-4a99b6005b05
# ╠═6c480c9e-9967-414b-b79b-05ae2a6d36db
# ╠═b1929e61-9e28-40fd-bece-b081ab38e91f
# ╠═982d8080-bf99-48a1-b649-ffb4a32b9783
